FROM telegraf:0.13

ADD telegraf.conf /etc/telegraf/telegraf.conf

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]
