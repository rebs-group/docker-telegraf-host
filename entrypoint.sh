#!/bin/bash
set -e

if [ -e /mnt/hostname ]; then
    HOST_HOSTNAME=$(cat /mnt/hostname)
    if [ -n "$HOST_HOSTNAME" ]; then
        HOSTNAME="$HOST_HOSTNAME"
    fi
    
    sed -i -e "s#%%%HOSTNAME%%%#$HOSTNAME#g" /etc/telegraf/telegraf.conf
fi

if [ "${1:0:1}" = '-' ]; then
    set -- telegraf "$@"
fi

exec "$@"
